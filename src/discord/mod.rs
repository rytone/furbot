use std::cell::RefCell;
use std::fmt;
use std::time::Duration;
use std::rc::Rc;

use hyper;
use hyper::header;
use hyper::header::Headers;
use hyper_rustls::HttpsConnector;

use lru_cache::LruCache;

use tokio_core::reactor::Handle;
use tokio_timer::{Timer, TimerError};
use futures;
use futures::{Future, IntoFuture, Sink, Stream};
use futures::sync::mpsc;

use serde::Serialize;
use serde::de::DeserializeOwned;
use serde_json;

use websocket;
use websocket::result::WebSocketError;

pub enum Token {
	Bot(String),
	#[allow(dead_code)] Bearer(String),
}

impl Token {
	fn raw(&self) -> String {
		match *self {
			Token::Bot(ref t) | Token::Bearer(ref t) => t.clone(),
		}
	}
}

impl fmt::Display for Token {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match *self {
			Token::Bot(ref t) => write!(f, "Bot {}", t),
			Token::Bearer(ref t) => write!(f, "Bearer {}", t),
		}
	}
}

#[derive(Debug)]
pub enum DiscordError {
	NetworkError(hyper::Error),
	NetStatusError(hyper::StatusCode),
	JSONError(serde_json::Error),
	WebsocketError(WebSocketError),
	HeartbeatError(TimerError),
	WSChannelError,
	WebsocketClosed(Option<websocket::message::CloseData>),
}

impl fmt::Display for DiscordError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match *self {
			DiscordError::NetworkError(ref e) => write!(f, "Network Error: {}", e),
			DiscordError::NetStatusError(ref e) => write!(f, "HTTP {}", e),
			DiscordError::JSONError(ref e) => write!(f, "JSON Error: {}", e),
			DiscordError::WebsocketError(ref e) => write!(f, "Websocket Error: {}", e),
			DiscordError::HeartbeatError(ref e) => write!(f, "Failed to tick heartbeat: {}", e),
			DiscordError::WSChannelError => write!(f, "Websocket Channel Error"),
			DiscordError::WebsocketClosed(ref d) => match *d {
				Some(ref c) => write!(f, "Websocket Closed ({})", c.status_code),
				None => write!(f, "Websocket Closed (no additional data)"),
			},
		}
	}
}

#[derive(Debug)]
pub enum WebsocketSendError<T> {
	SendError(mpsc::SendError<T>),
	NotConnected,
}

impl<T> fmt::Display for WebsocketSendError<T> {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match *self {
			WebsocketSendError::SendError(ref e) => write!(f, "Send Error: {}", e),
			WebsocketSendError::NotConnected => write!(f, "Not connected to websocket"),
		}
	}
}

#[must_use = "futures do nothing unless polled"]
pub struct DiscordResponse<V>(Box<Future<Item = V, Error = DiscordError> + 'static>);

impl<V> Future for DiscordResponse<V> {
	type Item = V;
	type Error = DiscordError;

	fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
		self.0.poll()
	}
}

pub trait EventHandler {
	fn ready(&mut self, _client: Rc<DiscordClient>) {}
	fn message_create(&mut self, _client: Rc<DiscordClient>, _m: self::model::Message) {}
}

pub struct DiscordClient {
	core_handle: Handle,
	token: Token,
	api_root: String,
	http: hyper::Client<HttpsConnector, hyper::Body>,
	websocket_sender: Option<mpsc::UnboundedSender<self::model::WSPacket>>,
	channel_cache: Rc<RefCell<LruCache<String, self::model::Channel>>>,
}

impl DiscordClient {
	pub fn new(token: Token, handle: &Handle) -> Self {
		let http = hyper::Client::configure()
			.connector(HttpsConnector::new(4, handle))
			.build(handle);

		DiscordClient {
			core_handle: handle.clone(),
			token,
			api_root: String::from("https://discordapp.com/api"),
			http,
			websocket_sender: None,
			channel_cache: Rc::new(RefCell::new(LruCache::new(50))),
		}
	}

	#[allow(dead_code)]
	pub fn set_api_root(&mut self, root: &str) {
		self.api_root = root.to_string();
	}

	fn build_headers(&self, headers: &mut Headers) {
		headers.set(header::Authorization(self.token.to_string()));
		headers.set(header::UserAgent::new(
			"DiscordBot (https://gitlab.com/rytone/furbot, 0.1.0)",
		));
	}

	fn build_request<B>(
		&self,
		method: hyper::Method,
		route: &str,
		body: Option<B>,
	) -> hyper::Request
	where
		B: Serialize,
	{
		let uri: hyper::Uri = (self.api_root.clone() + route).parse().unwrap();
		let mut req = hyper::Request::new(method, uri);

		{
			let headers = req.headers_mut();
			self.build_headers(headers);
		}

		if let Some(b) = body {
			let body_d = serde_json::to_vec(&b).unwrap();
			{
				let headers = req.headers_mut();
				headers.set(header::ContentLength(body_d.len() as u64));
				headers.set(header::ContentType::json());
			}
			req.set_body(body_d);
		}

		req
	}

	pub fn request<B, R>(
		&self,
		method: hyper::Method,
		route: &str,
		body: Option<B>,
	) -> DiscordResponse<R>
	where
		B: Serialize,
		R: DeserializeOwned + 'static,
	{
		let req = self.build_request(method, route, body);
		DiscordResponse(Box::new(
			self.http
				.request(req)
				.map_err(DiscordError::NetworkError)
				.and_then(|r| match r.status() {
					hyper::StatusCode::Ok => Ok(r),
					_ => Err(DiscordError::NetStatusError(r.status())),
				})
				.and_then(|r| r.body().concat2().map_err(DiscordError::NetworkError))
				.and_then(|body| serde_json::from_slice(&body).map_err(DiscordError::JSONError)),
		))
	}

	pub fn send_message(
		&self,
		channel_id: &str,
		content: &str,
	) -> DiscordResponse<self::model::Message> {
		#[derive(Serialize)]
		struct SendMessage {
			content: String,
		};

		self.request(
			hyper::Method::Post,
			&format!("/channels/{}/messages", channel_id),
			Some(SendMessage {
				content: content.to_string(),
			}),
		)
	}

	pub fn send_message_embed(
		&self,
		channel_id: &str,
		embed: self::model::embed::Embed,
	) -> DiscordResponse<self::model::Message> {
		#[derive(Serialize)]
		struct SendMessage {
			embed: self::model::embed::Embed,
		};

		self.request(
			hyper::Method::Post,
			&format!("/channels/{}/messages", channel_id),
			Some(SendMessage { embed }),
		)
	}

	pub fn edit_message(
		&self,
		channel_id: &str,
		message_id: &str,
		content: &str,
	) -> DiscordResponse<self::model::Message> {
		#[derive(Serialize)]
		struct EditMessage {
			content: String,
		};

		self.request(
			hyper::Method::Patch,
			&format!("/channels/{}/messages/{}", channel_id, message_id),
			Some(EditMessage {
				content: content.to_string(),
			}),
		)
	}

	pub fn get_channel(&self, channel_id: &str) -> DiscordResponse<self::model::Channel> {
		if let Some(channel) = self.channel_cache.borrow_mut().get_mut(channel_id) {
			DiscordResponse(Box::new(futures::future::ok(channel.clone())))
		} else {
			let channel_cache = Rc::clone(&self.channel_cache);
			let cache_id = String::from(channel_id);
			DiscordResponse(Box::new(self.request::<bool, self::model::Channel>(
				hyper::Method::Get,
				&format!("/channels/{}", channel_id),
				None,
			).map(move |channel| {
				channel_cache.borrow_mut().insert(cache_id, channel.clone());
				channel
			})))
		}
	}

	fn send_websocket(
		&self,
		m: self::model::WSPacket,
	) -> Result<(), WebsocketSendError<self::model::WSPacket>> {
		let tx = match self.websocket_sender.as_ref() {
			Some(s) => Ok(s),
			None => Err(WebsocketSendError::NotConnected),
		}?;

		tx.unbounded_send(m).map_err(WebsocketSendError::SendError)
	}

	pub fn update_game(
		&self,
		game_type: i32,
		game_name: &str,
		stream_url: Option<&str>,
	) -> Result<(), WebsocketSendError<self::model::WSPacket>> {
		self.send_websocket(self::model::WSPacket {
			op: 3,
			data: self::model::WSPacketData::UpdateStatus {
				since: None,
				game: Some(self::model::Game {
					name: game_name.to_string(),
					game_type,
					url: stream_url.map(|s| s.to_string()),
				}),
				status: "online".to_string(),
				afk: false,
			},
			last_seq: None,
			dispatch_name: None,
		})
	}

	pub fn run_websocket<T: EventHandler + 'static>(
		mut self,
		mut ev_handle: T,
	) -> DiscordResponse<()> {
		#[derive(Deserialize)]
		struct Gateway {
			url: String,
		}

		let (ws_tx, ws_rx) = mpsc::unbounded::<self::model::WSPacket>();
		self.websocket_sender = Some(ws_tx);

		let ws_core_handle = self.core_handle.clone();
		let raw_token = self.token.raw();
		let client_rc = Rc::new(self);
		let work = client_rc
			.request::<bool, Gateway>(hyper::Method::Get, "/gateway", None)
			.map(|g| format!("{}/?v=6&encoding=json", g.url))
			.and_then(move |g| {
				websocket::ClientBuilder::new(&g)
					.unwrap()
					.async_connect_secure(None, &ws_core_handle)
					.map_err(DiscordError::WebsocketError)
			})
			.and_then(move |(duplex, _)| {
				let (sink, stream) = duplex.split();
				stream
					.map_err(DiscordError::WebsocketError)
					.and_then(|m| match m {
						websocket::OwnedMessage::Close(e) => Err(DiscordError::WebsocketClosed(e)),
						_ => Ok(m),
					})
					.filter_map(|m| match m {
						websocket::OwnedMessage::Text(m) => Some(m),
						_ => None,
					})
					.into_future()
					.map_err(|(e, _)| e)
					.and_then(|(hello_msg, stream)| {
						serde_json::from_str::<self::model::WSPacket>(&hello_msg.unwrap())
							.map(|m| {
								let hb_interval = match m.data {
									self::model::WSPacketData::Hello { heartbeat_interval } => {
										heartbeat_interval
									}
									_ => panic!("first websocket message was not a hello"),
								};
								(hb_interval, stream)
							})
							.map_err(DiscordError::JSONError)
					})
					.and_then(move |(hb_int, stream)| {
						enum WSStream {
							Heartbeat,
							Message(self::model::WSPacket),
						};

						let hb_stream = Timer::default()
							.interval(Duration::from_millis(hb_int))
							.map(|_| WSStream::Heartbeat)
							.map_err(DiscordError::HeartbeatError);

						let identify = self::model::WSPacketData::Identify {
							token: raw_token,
							properties: self::model::ConnectionProperties {
								os: "windows".to_string(), // please un-hardcode kthx
								browser: "furbot".to_string(),
								device: "furbot".to_string(),
							},
							compress: false,
							large_threshold: 250,
						};

						let identify_packet = self::model::WSPacket {
							op: 2,
							data: identify,
							last_seq: None,
							dispatch_name: None,
						};
						serde_json::to_string(&identify_packet)
							.into_future()
							.map_err(DiscordError::JSONError)
							.and_then(|p| {
								sink.send(websocket::OwnedMessage::Text(p))
									.map_err(DiscordError::WebsocketError)
							})
							.and_then(move |sink| {
								let mut last_seq: Option<i32> = None;
								stream
									.and_then(|m| {
										serde_json::from_str::<self::model::WSPacket>(&m)
											.map_err(DiscordError::JSONError)
									})
									.map(WSStream::Message)
									.select(hb_stream)
									.filter_map(move |ws_ev| match ws_ev {
										WSStream::Heartbeat => {
											let packet = self::model::WSPacket {
												op: 1,
												data: self::model::WSPacketData::Heartbeat(
													last_seq,
												),
												last_seq: None,
												dispatch_name: None,
											};
											let data_ser = serde_json::to_string(&packet).unwrap();

											Some(websocket::OwnedMessage::Text(data_ser))
										}
										WSStream::Message(m) => {
											if let Some(ls) = m.last_seq {
												last_seq = Some(ls)
											}

											match m.data {
												self::model::WSPacketData::Heartbeat(_) => {
													if m.op == 1 {
														let packet = self::model::WSPacket {
															op: 1,
															data:
																self::model::WSPacketData::Heartbeat(
																	last_seq,
																),
															last_seq: None,
															dispatch_name: None,
														};
														let data_ser =
															serde_json::to_string(&packet).unwrap();

														Some(websocket::OwnedMessage::Text(
															data_ser,
														))
													} else {
														None
													}
												}
												self::model::WSPacketData::Other(d) => {
													if m.op == 0 {
														let dispatch_id = m.dispatch_name.unwrap();
														match dispatch_id.as_ref() {
															"READY" => ev_handle
																.ready(Rc::clone(&client_rc)),
															"MESSAGE_CREATE" => {
																let message: self::model::Message = serde_json::from_value(d).unwrap();
																ev_handle.message_create(
																	Rc::clone(&client_rc),
																	message,
																);
															}
															"CHANNEL_UPDATE" => {
																let channel: self::model::Channel = serde_json::from_value(d).unwrap();
																client_rc
																	.channel_cache
																	.borrow_mut()
																	.insert(
																		channel.id.clone(),
																		channel,
																	);
															}
															_ => (),
														}
													}
													None
												}
												_ => None,
											}
										}
									})
									.select(
										ws_rx
											.map_err(|_| DiscordError::WSChannelError)
											.and_then(|p| {
												serde_json::to_string(&p)
													.map_err(DiscordError::JSONError)
											})
											.map(websocket::OwnedMessage::Text),
									)
									.forward(sink.sink_map_err(DiscordError::WebsocketError))
									.map(|_| ())
							})
					})
			});
		DiscordResponse(Box::new(work))
	}
}

mod model;
pub use self::model::Message;
pub use self::model::embed;
