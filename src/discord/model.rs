use serde_json;

#[derive(Debug, Serialize, Deserialize)]
pub struct ConnectionProperties {
	#[serde(rename = "$os")] pub os: String,
	#[serde(rename = "$browser")] pub browser: String,
	#[serde(rename = "$device")] pub device: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum WSPacketData {
	Hello {
		heartbeat_interval: u64,
	},
	Identify {
		token: String,
		properties: ConnectionProperties,
		compress: bool,
		large_threshold: i32,
		// TODO shard, presence
	},
	Heartbeat(Option<i32>),
	UpdateStatus {
		since: Option<u64>,
		game: Option<Game>,
		status: String,
		afk: bool,
	},
	Other(serde_json::Value),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Game {
	pub name: String,
	#[serde(rename = "type")] pub game_type: i32,
	pub url: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct WSPacket {
	pub op: i32,
	#[serde(rename = "d")] pub data: WSPacketData,
	#[serde(rename = "s")] pub last_seq: Option<i32>,
	#[serde(rename = "t")] pub dispatch_name: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Message {
	pub id: String,
	pub channel_id: String,
	pub content: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Channel {
	pub id: String,
	#[serde(rename = "type")] pub channel_type: i32,
	pub nsfw: Option<bool>,
	pub guild_id: Option<String>,
}

pub mod embed {
	#[derive(Serialize)]
	pub struct Embed {
		pub title: String,
		pub url: String,
		pub footer: Footer,
		pub image: Image,
		pub fields: Vec<Field>,
	}

	#[derive(Serialize)]
	pub struct Footer {
		pub text: String,
	}

	#[derive(Serialize)]
	pub struct Image {
		pub url: String,
	}

	#[derive(Serialize)]
	pub struct Field {
		pub name: String,
		pub value: String,
	}
}
