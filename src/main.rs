extern crate chrono;
extern crate futures;
extern crate hyper;
extern crate hyper_rustls;
#[macro_use]
extern crate lazy_static;
extern crate lru_cache;
extern crate percent_encoding;
extern crate rand;
extern crate regex;
extern crate rusqlite;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tokio_core;
extern crate tokio_timer;
extern crate websocket;

use std::io;
use std::fmt;
use std::fs::File;
use std::path::Path;
use std::time::{Duration, Instant};
use std::thread;
use std::rc::Rc;
use std::cell::RefCell;

use tokio_core::reactor::{Core, Handle};
use futures::{Future, Stream};
use rand::Rng;
use regex::Regex;
use hyper_rustls::HttpsConnector;

use self::discord::{DiscordClient, EventHandler};
use self::discord::embed;

fn log_err(m: &str) {
    let time = chrono::Local::now().format("%H:%M");
    eprintln!(" (err@{}) {}", time, m)
}

fn log_info(m: &str) {
    let time = chrono::Local::now().format("%H:%M");
    eprintln!("(info@{}) {}", time, m)
}

#[derive(Debug, Deserialize)]
struct E621Post {
    id: u64,
    artist: Vec<String>,
    tags: String,
    score: i32,
    file_url: String,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "kind", content = "data")]
enum RedditEndpoint<T> {
    Listing(RedditListing<T>),
}

#[derive(Debug, Deserialize)]
struct RedditListing<T> {
    children: Vec<T>,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "kind", content = "data")]
enum RedditPost {
    #[serde(rename = "t3")] Link(RedditLinkData),
}

#[derive(Clone, Debug, Deserialize)]
struct RedditLinkData {
    title: String,
    score: i32,
    permalink: String,
    url: String,
    over_18: bool,
}

#[derive(Debug)]
enum E621Error {
    NetworkError(hyper::Error),
    NetStatusError(hyper::StatusCode),
    JSONError(serde_json::Error),
    DiscordError(discord::DiscordError),
}

impl fmt::Display for E621Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            E621Error::NetworkError(ref e) => e.fmt(f),
            E621Error::NetStatusError(ref e) => e.fmt(f),
            E621Error::JSONError(ref e) => e.fmt(f),
            E621Error::DiscordError(ref e) => e.fmt(f),
        }
    }
}

#[derive(Debug)]
enum RedditError {
    NetworkError(hyper::Error),
    NetStatusError(hyper::StatusCode),
    JSONError(serde_json::Error),
    DiscordError(discord::DiscordError),
    // MissingData,
    NoPosts,
}

impl fmt::Display for RedditError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            RedditError::NetworkError(ref e) => e.fmt(f),
            RedditError::NetStatusError(ref e) => e.fmt(f),
            RedditError::JSONError(ref e) => e.fmt(f),
            RedditError::DiscordError(ref e) => e.fmt(f),
            // RedditError::MissingData => write!(f, "missing data"),
            RedditError::NoPosts => write!(f, "no posts found"),
        }
    }
}

#[must_use = "futures do nothing unless polled"]
struct E621Result(Box<Future<Item = Vec<E621Post>, Error = E621Error> + 'static>);

impl Future for E621Result {
    type Item = Vec<E621Post>;
    type Error = E621Error;

    fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
        self.0.poll()
    }
}

#[must_use = "futures do nothing unless polled"]
struct RedditResult(Box<Future<Item = RedditLinkData, Error = RedditError> + 'static>);

impl Future for RedditResult {
    type Item = RedditLinkData;
    type Error = RedditError;

    fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
        self.0.poll()
    }
}

#[must_use = "futures do nothing unless polled"]
struct StubbedFuture(Box<Future<Item = (), Error = ()> + 'static>);

impl Future for StubbedFuture {
    type Item = ();
    type Error = ();

    fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
        self.0.poll()
    }
}

fn get_random_e621(
    http: &Rc<hyper::Client<HttpsConnector, hyper::Body>>,
    tags: &str,
) -> E621Result {
    let encoded_tags =
        percent_encoding::utf8_percent_encode(tags, percent_encoding::QUERY_ENCODE_SET);
    let uri: hyper::Uri = format!(
        "https://e621.net/post/index.json?tags={}&limit=1",
        encoded_tags
    ).parse()
        .unwrap();
    let mut req = hyper::Request::new(hyper::Method::Get, uri);
    {
        let headers = req.headers_mut();
        headers.set(hyper::header::UserAgent::new(
            "furbot/0.1.0 (https://gitlab.com/rytone/furbot)",
        ));
    }

    let work = http.request(req)
        .map_err(E621Error::NetworkError)
        .and_then(|r| match r.status() {
            hyper::StatusCode::Ok => Ok(r),
            _ => Err(E621Error::NetStatusError(r.status())),
        })
        .and_then(|r| r.body().concat2().map_err(E621Error::NetworkError))
        .and_then(|b| serde_json::from_slice::<Vec<E621Post>>(&b).map_err(E621Error::JSONError));

    E621Result(Box::new(work))
}

#[derive(Clone, Deserialize)]
struct OAuth {
    access_token: String,
    expires_in: i64,
}

#[derive(Clone)]
struct OAuthContext {
    time_recieved: chrono::DateTime<chrono::Local>,
    oauth: Option<OAuth>,
}

#[must_use = "futures do nothing unless polled"]
struct OAuthResult(Box<Future<Item = OAuth, Error = RedditError> + 'static>);

impl Future for OAuthResult {
    type Item = OAuth;
    type Error = RedditError;

    fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
        self.0.poll()
    }
}

#[derive(Clone)]
struct RedditCredentials {
    id: String,
    secret: String,
}

struct RedditClient {
    credentials: RedditCredentials,
    oauth: Rc<RefCell<OAuthContext>>,
    http: Rc<hyper::Client<HttpsConnector, hyper::Body>>,
}

fn load_reddit_credentials(path: &'static str) -> io::Result<RedditCredentials> {
    use io::Read;
    let mut file = File::open(path)?;
    let mut creds = String::new();
    file.read_to_string(&mut creds)?;
    let creds_split = creds.trim_right().split(',').map(String::from).collect::<Vec<String>>();
    Ok(RedditCredentials {
        id: creds_split[0].clone(),
        secret: creds_split[1].clone(),
    })
}

impl RedditClient {
    fn new(handle: &Handle) -> Self {
        RedditClient {
            credentials: load_reddit_credentials("./reddit_credentials.txt").unwrap(),
            oauth: Rc::new(RefCell::new(OAuthContext {
                time_recieved: chrono::Local::now(),
                oauth: None,
            })),
            http: Rc::new(
                hyper::Client::configure()
                    .connector(HttpsConnector::new(4, handle))
                    .build(handle),
            ),
        }
    }

    fn random_post(&self, query: String) -> RedditResult {
        let oauth_expire = self.oauth.borrow().time_recieved
            + chrono::Duration::seconds(
                self.oauth
                    .borrow()
                    .oauth
                    .as_ref()
                    .map(|o| o.expires_in)
                    .unwrap_or(-1),
            );
        let oauth_work =
            if self.oauth.borrow().oauth.is_none() || chrono::Local::now() >= oauth_expire {
                let uri: hyper::Uri = "https://www.reddit.com/api/v1/access_token"
                    .parse()
                    .unwrap();
                let mut req = hyper::Request::new(hyper::Method::Post, uri);
                req.set_body("grant_type=client_credentials");
                {
                    let headers = req.headers_mut();
                    let creds = self.credentials.clone();
                    headers.set(hyper::header::UserAgent::new(
                        "furbot/0.1.0 (https://gitlab.com/rytone/furbot)",
                    ));
                    headers.set(hyper::header::Authorization(hyper::header::Basic {
                        username: creds.id,
                        password: Some(creds.secret),
                    }));
                }
                let oauth_ctx_ref = Rc::clone(&self.oauth);
                OAuthResult(Box::new(
                    self.http
                        .request(req)
                        .map_err(RedditError::NetworkError)
                        .and_then(|r| match r.status() {
                            hyper::StatusCode::Ok => Ok(r),
                            _ => Err(RedditError::NetStatusError(r.status())),
                        })
                        .and_then(|r| r.body().concat2().map_err(RedditError::NetworkError))
                        .and_then(|b| {
                            serde_json::from_slice::<OAuth>(&b).map_err(RedditError::JSONError)
                        })
                        .map(move |o| {
                            let ctx = OAuthContext {
                                time_recieved: chrono::Local::now(),
                                oauth: Some(o.clone()),
                            };
                            oauth_ctx_ref.replace(ctx);
                            o
                        }),
                ))
            } else {
                OAuthResult(Box::new(futures::future::ok::<OAuth, RedditError>(
                    self.oauth.borrow().oauth.as_ref().unwrap().clone(),
                )))
            };

        let http = Rc::clone(&self.http);
        let work = oauth_work
            .and_then(move |oauth| {
                let encoded_search = percent_encoding::utf8_percent_encode(
                    &query,
                    percent_encoding::QUERY_ENCODE_SET,
                );
                let uri: hyper::Uri = format!(
                "https://oauth.reddit.com/search.json?q={}&sort=hot&limit=50&type=link&t=day&raw_json=1",
                encoded_search
            ).parse()
                .unwrap();
                let mut req = hyper::Request::new(hyper::Method::Get, uri);
                {
                    let headers = req.headers_mut();
                    headers.set(hyper::header::UserAgent::new(
                        "furbot/0.1.0 (https://gitlab.com/rytone/furbot)",
                    ));
                    headers.set(hyper::header::Authorization(format!(
                        "bearer {}",
                        oauth.access_token
                    )));
                }
                http.request(req).map_err(RedditError::NetworkError)
            })
            .and_then(|r| match r.status() {
                hyper::StatusCode::Ok => Ok(r),
                _ => Err(RedditError::NetStatusError(r.status())),
            })
            .and_then(|r| r.body().concat2().map_err(RedditError::NetworkError))
            .and_then(|b| {
                serde_json::from_slice::<RedditEndpoint<RedditPost>>(&b)
                    .map_err(RedditError::JSONError)
            })
            .and_then(|res| match res {
                RedditEndpoint::Listing(listing) => {
                    let posts = listing
                        .children
                        .iter()
                        .filter_map(|post| match *post {
                            RedditPost::Link(ref data) => Some(data.clone()),
                            // _ => None
                        })
                        .filter(|post| post.score > 100)
                        .collect::<Vec<RedditLinkData>>();
                    match rand::thread_rng().choose(&posts) {
                        Some(p) => Ok(p.clone()),
                        None => Err(RedditError::NoPosts),
                    }
                } // _ => Err(RedditError::MissingData)
            });

        RedditResult(Box::new(work))
    }
}

fn owoify(text: &str) -> String {
    lazy_static! {
        static ref FACE_VEC: Vec<&'static str> = vec!["(・\\`ω´・)", ";;w;;", "owo", "UwU", ">w<", "^w^"];

        static ref W_LOWER: Regex = Regex::new("(?:r|l)").unwrap();
        static ref W_UPPER: Regex = Regex::new("(?:R|L)").unwrap();

        static ref NY_LOWER: Regex  = Regex::new("n([aeiou])").unwrap();
        static ref NY_BOTH_1: Regex = Regex::new("N([aeiou])").unwrap();
        static ref NY_BOTH_2: Regex = Regex::new("n([AEIOU])").unwrap();
        static ref NY_UPPER: Regex  = Regex::new("N([AEIOU])").unwrap();

        static ref OVE: Regex = Regex::new("ove").unwrap();
        static ref FACES: Regex = Regex::new("!+").unwrap();
    }

    let text = W_LOWER.replace_all(text, "w");
    let text = W_UPPER.replace_all(&text, "W");
    let text = NY_LOWER.replace_all(&text, "ny$1");
    let text = NY_BOTH_1.replace_all(&text, "Ny$1");
    let text = NY_BOTH_2.replace_all(&text, "nY$1");
    let text = NY_UPPER.replace_all(&text, "NY$1");
    let text = OVE.replace_all(&text, "uv");
    let text = FACES.replace_all(&text, |_: &regex::Captures| {
        format!(" {} ", rand::thread_rng().choose(&FACE_VEC).unwrap())
    });

    text.to_string()
}

struct Bot {
    core_handle: Handle,
    http: Rc<hyper::Client<HttpsConnector, hyper::Body>>,
    reddit: Rc<RedditClient>,
    db: Rc<rusqlite::Connection>,
}

impl Bot {
    fn new(handle: &Handle) -> Self {
        let http = hyper::Client::configure()
            .connector(HttpsConnector::new(4, handle))
            .build(handle);

        let db = rusqlite::Connection::open(Path::new("./owo.db")).expect("failed to open owo db");
        db.execute(
            "CREATE TABLE IF NOT EXISTS owos (guild TEXT PRIMARY KEY, count INTEGER)",
            &[],
        ).expect("failed to ensure db table");

        Bot {
            core_handle: handle.clone(),
            http: Rc::new(http),
            reddit: Rc::new(RedditClient::new(&handle)),
            db: Rc::new(db),
        }
    }
}
impl EventHandler for Bot {
    fn ready(&mut self, client: Rc<DiscordClient>) {
        log_info("furbot online");
        if client.update_game(2, "pepper coyote", None).is_err() {
            log_err("somehow failed to update presence");
        }
    }

    fn message_create(&mut self, client: Rc<DiscordClient>, m: self::discord::Message) {
        if m.content.to_lowercase().contains("owo") {
            let db = Rc::clone(&self.db);
            let client = Rc::clone(&client);
            let id = m.channel_id.clone();
            let work = client
                .get_channel(&m.channel_id.clone())
                .map_err(|e| log_err(&format!("failed to get channel for owo check: {}", e)))
                .and_then(move |c| {
                    if let Some(guild_id) = c.guild_id {
                        let owo_count = db.query_row(
                            "SELECT count FROM owos WHERE guild=?1",
                            &[&guild_id],
                            |row| row.get(0),
                        );
                        let new_count: rusqlite::Result<i32> = match owo_count {
                            Ok::<i32, rusqlite::Error>(count) => {
                                let new_count: i32 = count + 1;
                                let update_res = db.execute(
                                    "UPDATE owos SET count=?2 WHERE guild=?1",
                                    &[&guild_id, &new_count],
                                );
                                if let Err(e) = update_res {
                                    log_err(&format!("failed to update owo row: {}", e));
                                    Err(e)
                                } else {
                                    Ok(new_count)
                                }
                            }
                            Err(rusqlite::Error::QueryReturnedNoRows) => {
                                let new_count: i32 = 1;
                                let insert_res = db.execute(
                                    "INSERT INTO owos (guild, count) VALUES (?1, ?2)",
                                    &[&guild_id, &new_count],
                                );
                                if let Err(e) = insert_res {
                                    log_err(&format!("failed to insert new owo row: {}", e));
                                    Err(e)
                                } else {
                                    Ok(new_count)
                                }
                            }
                            Err(e) => {
                                log_err(&format!("failed to get owo count: {}", e));
                                Err(e)
                            }
                        };
                        match new_count {
                            Ok(new_count) => {
                                if new_count % 100 == 0 {
                                    let msg_send = client.send_message(&id, &format!("OwO whats this? this is the {}th time someone has owoed in this server!", new_count))
                                        .map(|_| ())
                                        .map_err(|e| log_err(&format!("failed to send owo message: {}", e)));
                                    StubbedFuture(Box::new(msg_send))
                                } else {
                                    StubbedFuture(Box::new(futures::future::ok::<(), ()>(())))
                                }
                            }
                            Err(e) => {
                                log_err(&format!("failed to update owo count: {}", e));
                                StubbedFuture(Box::new(futures::future::ok::<(), ()>(())))
                            }
                        }
                    } else {
                        StubbedFuture(Box::new(futures::future::ok::<(), ()>(())))
                    }
                });
            self.core_handle.spawn(work);
        }

        let command = m.content.splitn(3, ' ').collect::<Vec<&str>>();
        if let Some(pfx) = command.get(0) {
            if &pfx.to_lowercase() == "furbot" {
                if let Some(cmd) = command.get(1) {
                    match cmd.to_lowercase().as_str() {
                        "ping" => {
                            let st = Instant::now();
                            self.core_handle.spawn(
                                client
                                    .send_message(&m.channel_id, "...")
                                    .and_then(move |m| {
                                        let elapsed = st.elapsed();
                                        let whole_ms = (elapsed.as_secs() as f64) * 1000.0;
                                        let sub_ms = f64::from(elapsed.subsec_nanos()) / 1e6;
                                        client.edit_message(
                                            &m.channel_id,
                                            &m.id,
                                            &format!(
                                                "pong!! ^w^ (compweted in {:.2}ms OwO)",
                                                whole_ms + sub_ms
                                            ),
                                        )
                                    })
                                    .map(|_| ())
                                    .map_err(|e| log_err(&format!("failed to send pong: {}", e))),
                            );
                        }
                        "zander" => self.core_handle.spawn(
                            client
                                .send_message(&m.channel_id, "zandew is gayyy >w<")
                                .map(|_| ())
                                .map_err(|e| log_err(&format!("failed to call zander gay: {}", e))),
                        ),
                        "owoify" => if let Some(text) = command.get(2) {
                            let owoified = owoify(text);
                            self.core_handle.spawn(
                                client
                                    .send_message(&m.channel_id, &owoified)
                                    .map(|_| ())
                                    .map_err(|e| {
                                        log_err(&format!("failed to send owoified message: {}", e))
                                    }),
                            );
                        },
                        "about" => self.core_handle.spawn(
                            client
                                .send_message(&m.channel_id, "furbot is an experimental discord bot made with a custom api wrapper written in rust and based entirely on `futures-rs`. the goal is to make something that runs with a stable compiler, so `futures-await` is nowhere to be found. source code is available at https://gitlab.com/rytone/furbot for those who dare.")
                                .map(|_| ())
                                .map_err(|e| log_err(&format!("failed to send about: {}", e)))
                        ),
                        "no" | "stop" | "shutdown" | "delet" => self.core_handle.spawn(
                            client.send_message(&m.channel_id, "no")
                                .map(|_| ())
                                .map_err(|e| log_err(&format!("failed to no: {}", e)))
                        ),
                        "surprise" => {
                            let http = Rc::clone(&self.http);
                            let success_m = m.clone();
                            let fail_m = m.clone();
                            let client_success_ref = Rc::clone(&client);
                            let client_error_ref = Rc::clone(&client);
                            let work = client.get_channel(&m.channel_id)
                                .map_err(|e| {
                                    log_err(&format!("failed to get channel: {}", e));
                                    E621Error::DiscordError(e)
                                })
                                .and_then(move |channel| {
                                    let tags = if channel.nsfw.unwrap_or(false) {
                                        "-mlp -flash order:random score:>50"
                                    } else {
                                        "-mlp -flash order:random score:>50 rating:s"
                                    };
                                    get_random_e621(&http, tags)
                                })
                                .and_then(move |posts| if let Some(post) = posts.get(0) {
                                    let mut tags = post.tags.split(' ').collect::<Vec<&str>>();
                                    let orig_len = tags.len();
                                    tags.truncate(10);
                                    let shortened_len = orig_len.saturating_sub(tags.len());
                                    client_success_ref.send_message_embed(
                                        &success_m.channel_id,
                                        embed::Embed {
                                            title: format!("#{}", post.id),
                                            url: format!("https://e621.net/post/show/{}", post.id),
                                            footer: embed::Footer {
                                                text: format!("{} points on e621", post.score),
                                            },
                                            image: embed::Image {
                                                url: post.file_url.clone(),
                                            },
                                            fields: vec!(embed::Field {
                                                name: post.artist.join(", "),
                                                value: format!("`{}` (+ {} more)", tags.join(" "), shortened_len),
                                            }),
                                        }
                                    )
                                } else {
                                    client_success_ref.send_message(&success_m.channel_id, "no posts found uwu")
                                }.map(|_| ()).map_err(E621Error::DiscordError))
                                .or_else(move |e| {
                                    let maybe_msg = match e {
                                        E621Error::NetworkError(e) => Some(format!(">w< an internal network error occured: {}", e)),
                                        E621Error::NetStatusError(e) => Some(format!("uwu e621 returned an error ({})", e)),
                                        E621Error::JSONError(_) => Some("UwU e621 returned invalid data (it may be down >W<)".to_string()),
                                        E621Error::DiscordError(e) => {
                                            log_err(&format!("discord failed while sending surprise: {}", e));
                                            None
                                        }
                                    };
                                    if let Some(msg) = maybe_msg {
                                        StubbedFuture(Box::new(client_error_ref.send_message(&fail_m.channel_id, &msg)
                                            .map(|_| ())
                                            .map_err(|e| log_err(&format!("failed to send surprise error: {}", e)))))
                                    } else {
                                        StubbedFuture(Box::new(futures::future::ok::<(), ()>(())))
                                    }
                                });
                            self.core_handle.spawn(work.map(|_| ()));
                        },
                        "search" => {
                            if command.get(2) != None && command.get(2) != Some(&"") {
                                let http = Rc::clone(&self.http);
                                let success_m = m.clone();
                                let fail_m = m.clone();
                                let client_success_ref = Rc::clone(&client);
                                let client_error_ref = Rc::clone(&client);
                                let terms = command[2].to_string();
                                let work = client.get_channel(&m.channel_id)
                                    .map_err(|e| {
                                        log_err(&format!("failed to get channel: {}", e));
                                        E621Error::DiscordError(e)
                                    })
                                    .and_then(move |channel| {
                                        let tags = if channel.nsfw.unwrap_or(false) {
                                            format!("{} order:random score:>20", terms)
                                        } else {
                                            format!("{} order:random score:>20 rating:s", terms)
                                        };
                                        get_random_e621(&http, &tags)
                                    })
                                    .and_then(move |posts| if let Some(post) = posts.get(0) {
                                        let mut tags = post.tags.split(' ').collect::<Vec<&str>>();
                                        let orig_len = tags.len();
                                        tags.truncate(10);
                                        let shortened_len = orig_len.saturating_sub(tags.len());
                                        client_success_ref.send_message_embed(
                                            &success_m.channel_id,
                                            embed::Embed {
                                                title: format!("#{}", post.id),
                                                url: format!("https://e621.net/post/show/{}", post.id),
                                                footer: embed::Footer {
                                                    text: format!("{} points on e621", post.score),
                                                },
                                                image: embed::Image {
                                                    url: post.file_url.clone(),
                                                },
                                                fields: vec!(embed::Field {
                                                    name: post.artist.join(", "),
                                                    value: format!("`{}` (+ {} more)", tags.join(" "), shortened_len),
                                                }),
                                            }
                                        )
                                    } else {
                                        client_success_ref.send_message(&success_m.channel_id, "no posts found uwu")
                                    }.map(|_| ()).map_err(E621Error::DiscordError))
                                    .or_else(move |e| {
                                        let maybe_msg = match e {
                                            E621Error::NetworkError(e) => Some(format!(">w< an internal network error occured: {}", e)),
                                            E621Error::NetStatusError(e) => match e {
                                                hyper::StatusCode::UnprocessableEntity => Some("OwO too many tags".to_string()),
                                                _ => Some(format!("uwu e621 returned an error ({})", e)),
                                            },
                                            E621Error::JSONError(_) => Some("UwU e621 returned invalid data (it may be down >W<)".to_string()),
                                            E621Error::DiscordError(e) => {
                                                log_err(&format!("discord failed while sending search result: {}", e));
                                                None
                                            }
                                        };
                                        if let Some(msg) = maybe_msg {
                                            StubbedFuture(Box::new(client_error_ref.send_message(&fail_m.channel_id, &msg)
                                                .map(|_| ())
                                                .map_err(|e| log_err(&format!("failed to send search error: {}", e)))))
                                        } else {
                                            StubbedFuture(Box::new(futures::future::ok::<(), ()>(())))
                                        }
                                    });
                                self.core_handle.spawn(work.map(|_| ()));
                            }
                        },
                        "irl" => {
                            let reddit = Rc::clone(&self.reddit);
                            let success_m = m.clone();
                            let fail_m = m.clone();
                            let client_success_ref = Rc::clone(&client);
                            let client_error_ref = Rc::clone(&client);
                            let work = client.get_channel(&m.channel_id)
                                .map_err(|e| {
                                    log_err(&format!("failed to get channel: {}", e));
                                    RedditError::DiscordError(e)
                                })
                                .and_then(move |channel| {
                                    let search = if channel.nsfw.unwrap_or(false) {
                                        "subreddit:furry_irl"
                                    } else {
                                        "subreddit:furry_irl nsfw:no"
                                    };
                                    //get_random_reddit(&http, search.0, search.1)
                                    reddit.random_post(String::from(search))
                                })
                                .and_then(move |post| {
                                    client_success_ref.send_message_embed(
                                        &success_m.channel_id,
                                        embed::Embed {
                                            title: post.title.clone(),
                                            url: format!("https://reddit.com{}", post.permalink),
                                            footer: embed::Footer {
                                                text: format!("{} points on /r/furry_irl", post.score),
                                            },
                                            image: embed::Image {
                                                url: post.url.clone(),
                                            },
                                            fields: vec!(),
                                        }
                                    ).map_err(RedditError::DiscordError)
                                })
                                .map(|_| ())
                                .or_else(move |e| {
                                    let maybe_msg = match e {
                                        RedditError::NetworkError(e) => Some(format!(">w< an internal network error occured: {}", e)),
                                        RedditError::NetStatusError(e) => Some(format!("uwu reddit returned an error ({})", e)),
                                        RedditError::JSONError(_) /*| RedditError::MissingData */=> Some("UwU reddit returned invalid data (it may be down >W<)".to_string()),
                                        RedditError::NoPosts => Some("no posts found uwu".to_string()),
                                        RedditError::DiscordError(e) => {
                                            log_err(&format!("discord failed while sending irl: {}", e));
                                            None
                                        }
                                    };
                                    if let Some(msg) = maybe_msg {
                                        StubbedFuture(Box::new(client_error_ref.send_message(&fail_m.channel_id, &msg)
                                            .map(|_| ())
                                            .map_err(|e| log_err(&format!("failed to send irl error: {}", e)))))
                                    } else {
                                        StubbedFuture(Box::new(futures::future::ok::<(), ()>(())))
                                    }
                                });
                            self.core_handle.spawn(work.map(|_| ()));
                        },
                        _ => (),
                    }
                }
            }
        }
    }
}

fn load_token(path: &'static str) -> io::Result<String> {
    use io::Read;
    let mut file = File::open(path)?;
    let mut token = String::new();
    file.read_to_string(&mut token)?;
    Ok(token.trim_right().to_string())
}

fn main() {
    loop {
        let mut core = Core::new().unwrap();
        let token = discord::Token::Bot(load_token("token.txt").expect("token"));

        let client = DiscordClient::new(token, &core.handle());
        let work = client.run_websocket(Bot::new(&core.handle()));

        if let Err(e) = core.run(work) {
            log_err(&format!("error while running: {}", e));
        }
       
        thread::sleep(Duration::from_secs(30));
    }
}

mod discord;
